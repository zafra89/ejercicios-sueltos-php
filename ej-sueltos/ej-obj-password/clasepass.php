<?php
class PasswordValidator {

  function isValid($password) {
    if (strlen($password) >= 8 && strlen($password) < 10 && preg_match('/[.,_;-]/', $password)) {
      return 'basica';
    } else if (strlen($password) >= 10 && strlen($password) < 12 && preg_match('/[.,_;-]/', $password)) {
      return 'media';
    } else if (strlen($password) >= 12 && preg_match('/[.,_;-]/', $password)) {
      return 'alta';
    }
  }
}
?>