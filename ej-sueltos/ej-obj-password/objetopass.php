<?php
require_once 'clasepass.php';
$inputPass = $_POST['pass'];
$myPassword = new PasswordValidator();

if ($myPassword->isValid($inputPass) == 'basica') {
  echo 'La contraseña es de nivel básico';
} else if ($myPassword->isValid($inputPass) == 'media') {
  echo 'La contraseña es de nivel medio';
} else if ($myPassword->isValid($inputPass) == 'alta') {
  echo 'La contraseña es de nivel alto';
} else {
  echo 'La contraseña No es válida';
}
?>