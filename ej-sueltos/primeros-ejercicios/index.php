  <!DOCTYPE html>
  <html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
  </head>
  <body>
    <?php

    // 1) Meter los elementos de una array en un ul como li:
    $colores = ['verde', 'blanco', 'rojo', 'azul', 'amarillo'];

    foreach ($colores as $clave => $color) {
      if ($clave == 0) {
        echo '<ul><li>' . $color . '</li>';
      } else if ($clave == count($colores)) {
        echo '<li' . $color . '</li></ul>';
      } else {
        echo '<li>' . $color . '</li>';
      }
    }

    // 2) Acceder a la capital de un país introduciendo el país en la URL: ¡¡¡NO TERMINADO!!!
    echo '<br>';
    $paisesCapitales = ['Alemania' => 'Berlín', 'Argentina' => 'Buenos Aires', 'Bolivia' => 'La Paz', 'Camerún' => 'Yaundé', 'Dinamarca' => 'Copenhague', 'España' => 'Madrid', 'Eslovenia' => 'Liubliana'];

    //3) Obtener la palabra más larga y la más corta de una array:
    echo '<br>';
    $palabras = ['sal', 'pimienta', ' aceite', 'cebolla', 'te', 'azúcar', 'agua'];

    $larga = $palabras[0];
    $corta = $palabras[0];

    for ($i = 0; $i < count($palabras); $i++) {
      if (strlen($larga) < strlen($palabras[$i])) {
        $larga = $palabras[$i];
      }
      if (strlen($corta) > strlen($palabras[$i])) {
        $corta = $palabras[$i];
      }
    }
    echo 'La palabra más larga es: ' . $larga . ' y la más corta es: ' . $corta . '<br>';

    // 4) Contruir una pirámide de *:
    echo '<br>';
    $item = '*';
    $contador = 1;
    $alturaPiramide = 10;
    $indentacion = $alturaPiramide * 2;
    $espacio = '&nbsp;';
    for ($i = 0; $i < $alturaPiramide; $i++) {
      echo str_repeat($espacio, $indentacion) . str_repeat($item, $contador)  . '<br>';
      $contador += 2;
      $indentacion -= 2;
    }

    // 5) Transformar el nombre del fichero a minúsculas o mayúsculas dependiendo de si su extensión es .db o .exe:
    echo '<br>';
    $fichero = 'ANA.db';
    if (strpos($fichero, '.exe')) {
      echo "Como el fichero " . $fichero . " tiene una extensión .exe ahora se llama " . strtoupper($fichero) . "<br>";
    } else if (strpos($fichero, '.db')) {
      echo "Como el fichero " . $fichero . " tiene una extensión .db ahora se llama " . strtolower($fichero) . "<br>";
    } else {
      echo "El archivo no tiene una extensión .exe ni .db";
    }

    // 6) Determinar si una palabra es palíndromo o si un número es capicúo:
    echo '<br>';
    $elemento = "abba";
    $elementoVolteado = strrev($elemento);
    if ($elemento == $elementoVolteado) {
      echo "<br> El elemento " . $elemento . " es PALÍNDROMO/CAPICÚO";
    } else {
      echo "<br> El elemento " . $elemento . " NO es palíndromo/capicúo";
    }
    ?>
  </body>
  </html>