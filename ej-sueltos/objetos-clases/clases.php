<?php
  class Vehiculo {

    var $kilometros;
    var $litrosCombustible;

    function __construct($comb, $rued, $mar, $ti, $pas) {
      $this->tipoCombustible = $comb;
      $this->numRuedas = $rued;
      $this->marca = $mar;
      $this->modelo = $ti;
      $this->pasajeros = $pas;

      $this->kilometros = 0;
      $this->litrosCombustible = 0;
    }

    function setLitrosCombustible($combustible) {
      $this->litrosCombustible = $combustible;
    }

    function getLitrosCombustible() {
      return $this->litrosCombustible;
    }

    function mover($kilometros) {
      $this->kilometros += $kilometros;
      $this->litrosCombustible -= ($kilometros * 5) / 100;
    }

    function repostar($litros) {
      $this->litrosCombustible += $litros;
    }
  }
?>
