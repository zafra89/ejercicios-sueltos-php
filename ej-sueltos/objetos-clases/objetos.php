<?php
require_once 'clases.php';
$miVehiculo1 = new Vehiculo('diesel', 4, 'Mercedes', 'MX5', 5);
$miVehiculo2 = new Vehiculo('gasolina', 2, 'Kawasaki', 'Xtrme', 2);
$miVehiculo1->repostar(50);
$miVehiculo1->mover(308);
echo 'Después de mi viaje me quedan ' . $miVehiculo1->getLitrosCombustible() . ' litros de combustible';
?>