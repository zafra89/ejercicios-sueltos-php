<?php
$palabra = strtolower($_POST['palabra']);
$file = $_FILES['fichero'];
$contenido = strtolower(file_get_contents($file['tmp_name']));
$resultado = substr_count($contenido, $palabra);

echo 'La palabra ' . $palabra . ' aparece ' . $resultado . ' veces en El Quijote';
?>